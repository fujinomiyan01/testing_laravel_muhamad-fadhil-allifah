@include('templates.header')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
        @yield('contents')
  </div>
  <!-- /.content-wrapper -->

@include('templates.footer')
