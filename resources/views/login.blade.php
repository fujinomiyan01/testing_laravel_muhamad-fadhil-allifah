<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="ZiePOS">
    <meta name="author" content="ZiePOS">
    <meta name="keywords" content="ZiePOS">
    <link rel="icon" href="{{ asset('assets') }}/img/ziepos.png" type="image/png">


    <!-- Title Page-->
    <title>Login | Aplikasi Penduduk</title>

    <!-- Icons font CSS-->
    <link href="{{ asset('templates') }}/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="{{ asset('templates') }}/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="{{ asset('templates') }}/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="{{ asset('templates') }}/vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="{{ asset('templates') }}/css/main.css" rel="stylesheet" media="all">

    {{-- Toastr --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" referrerpolicy="no-referrer" />

</head>

<body style="">
    <div class="page-wrapper bg-blue p-t-100 p-b-100 font-robo">
        <div class="wrapper wrapper--w680">
            <div class="card card-1">
                <div class="card-body">
                    <h2 class="title">Login</h2>
                    
                    <form method="POST" enctype="multipart/form-data" action="{{ route('login') }}">
                        @csrf
                        @method('post')
                        @if(session()->has('success'))
                            {{ Session::get('success') }}
                        @endif
                        <div class="input-group">
                            <input class="input--style-1" id="email" required type="email" placeholder="EMAIL USER" name="email">
                        </div>
                        <div class="input-group">
                            <input class="input--style-1" id="password" required type="password" placeholder="PASSWORD USER"  name="password">
                        </div>
                        <div class="p-t-20">
                            <button class="btn btn--radius btn--green" type="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Jquery JS-->
    <script src="{{ asset('templates') }}/vendor/jquery/jquery.min.js"></script>
    <!-- Vendor JS-->
    <script src="{{ asset('templates') }}/vendor/select2/select2.min.js"></script>
    <script src="{{ asset('templates') }}/vendor/datepicker/moment.min.js"></script>
    <script src="{{ asset('templates') }}/vendor/datepicker/daterangepicker.js"></script>

    <!-- Main JS-->
    <script src="{{ asset('templates') }}/js/global.js"></script>

    {{-- Toastr --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
<!-- end document-->
