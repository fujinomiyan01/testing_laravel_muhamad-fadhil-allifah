@extends('templates.layout')

@section('title')
    <title>Users CRUD</title>
@endsection

@section('page')
    Users CRUD
@endsection

@section('breadcrumb')
@parent
    Users CRUD
@endsection

@push('styles')
    
@endpush

@section('contents')
<section class="content">
    <div class="row mx-3">
        <div class="col-md-12 p-2 mb-3" style="background-color: white">
            <div class="box mb-4">
                <div class="box-body table-responsive ">
                    <h2 class="text-center mt-3 mb-2">Set Satuan</h2>
                    <button type="button" class="btn btn-primary ml-4 mb-4 mt-3" data-toggle="modal" data-target="#formModalSatuan">
                        <i class="fas fa-plus"></i>&nbsp; Tambah Data
                    </button>
                    <div class="col-lg-12">
                        <div class="table-responsive p-3">
                            @include('satuan.data')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('users.form')
@endsection

@push('scripts')
@endpush