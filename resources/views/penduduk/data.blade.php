<div class="box-body table-responsive">
    <table class="table table-striped table-hover table-bordered dt-responsive" style="width: 100%;" id="tbl-data-satuan">
        <thead class="table-success">
            <tr>
                <td width="9%" class="text-center" width="10%">No</td>
                <td class="text-center">Nama Penduduk</td>
                <td class="text-center">Gender</td>
                <td class="text-center">Tempat Lahir</td>
                <td class="text-center">Tanggal Lahir</td>
                <td class="text-center">Alamat</td>
                <td class="text-center">Photo</td>
                <td class="text-center">Pegawai Yang Menginput</td>
                <td class="text-center">Action</td>
            </tr>
        </thead>
        <tbody>
            <p style="visibility: hidden">{{ $no = 1 }}</p>
            @foreach ($penduduk as $item)
            <tr>
                <td class="text-center">{{ $no++}}</td>
                <td>{{ $item->name }}</td>
                <td>{{ $item->gender }}</td>
                <td>{{ $item->place_of_birth }}</td>
                <td>{{ $item->date_of_birth }}</td>
                <td>{{ $item->address }}</td>
                <td><img src="{{ $item->photo }}" alt="{{ $item->name }}" width="100"></td>
                <td>{{ $item->nama_user }}</td>
                <td>
                    <button class="btn btn-xs btn-success" type="button" title="Edit" data-mode="edit"
                            data-toggle="modal" data-target="#formModal" data-id_penduduk="{{ $item->id }}"
                            data-name="{{ $item->name }}" data-gender="{{ $item->gender }}" data-place_of_birth="{{ $item->place_of_birth }}" data-date_of_birth="{{ $item->date_of_birth }}" data-address="{{ $item->address }}" data-photo="{{ $item->photo }}" data-route="{{ route('penduduk.update', $item->id) }}">
                            <i class="fas fa-edit"></i>
                    </button>
                    <form action="{{ route('penduduk.destroy', $item->id) }}" style="display: inline;" method="post">
                        @csrf
                        @method('DELETE')
                        <button onclick="deleteData('{{ route('penduduk.destroy', $item->id) }}')" class="btn btn-xs btn-danger delete-data" type="button" title="Delete">
                            <i class="fas fa-trash"></i>
                        </button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>