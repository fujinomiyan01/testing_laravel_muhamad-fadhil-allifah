@extends('templates.layout')

@section('contents')
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Penduduk Page</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Penduduk Page</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Penduduk</h3>
      </div>
      <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>                    
                @endforeach
              </ul>
            </div>
        @endif
        @if(session()->has('success'))
          <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            {{ Session::get('success') }}
          </div>
        @endif
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#formModal">
              <i class="fas fa-plus"></i>&nbsp; Tambah Data
          </button>
          <div class="col-lg-12">
              <div class="table-responsive p-3">
                  @include('penduduk.data')
              </div>
          </div>
      </div>
      <!-- /.card-footer-->
    </div>
    <!-- /.card -->

  </section>
  @include('penduduk.form')
@endsection

@push('scripts')
  <script>
      function deleteData(url) {
              Swal.fire({
                  title: 'Hapus Penduduk yang dipilih?',
                  icon: 'question',
                  iconColor: '#DC3545',
                  showDenyButton: true,
                  denyButtonColor: '#838383',
                  denyButtonText: 'Batal',
                  confirmButtonText: 'Hapus',
                  confirmButtonColor: '#DC3545'
                  }).then((result) => {
                  if (result.isConfirmed) {
                      $.post(url, {
                          '_token': $('[name=csrf-token]').attr('content'),
                          '_method': 'delete'
                      })
                      .done((response) => {
                          Swal.fire({
                              title: 'Sukses!',
                              text: 'Penduduk berhasil dihapus',
                              icon: 'success',
                              confirmButtonText: 'Lanjut',
                              confirmButtonColor: '#28A745'
                          }) 
                          location.reload();
                      })
                      .fail((errors) => {
                          Swal.fire({
                              title: 'Gagal!',
                              text: 'Penduduk tidak bisa dihapus',
                              icon: 'error',
                              confirmButtonText: 'Kembali',
                              confirmButtonColor: '#DC3545'
                          })                       
                          return;
                      });
                  } else if (result.isDenied) {
                      Swal.fire({
                          title: 'Penduduk batal dihapus',
                          icon: 'warning',
                      })
                  }
              })
          }

          $('#formModal').on("show.bs.modal", function(e){
              const btn = $(e.relatedTarget)
              const id_penduduk = btn.data('id_penduduk')
              const name = btn.data('name')
              const gender = btn.data('gender')
              const place_of_birth = btn.data('place_of_birth')
              const date_of_birth = btn.data('date_of_birth')
              const address = btn.data('address')
              const photo = btn.data('photo')
              const mode = btn.data('mode')
              const modal = $(this)
              const url = btn.data('route')
          
              if(mode === 'edit'){
                  modal.find('#modal-title').text("Edit Data Penduduk")
                  modal.find('.modal-body #name').val(name)
                  modal.find('.modal-body #gender').val(gender)
                  modal.find('.modal-body #place_of_birth').val(place_of_birth)
                  modal.find('.modal-body #date_of_birth').val(date_of_birth)
                  // document.getElementById("address").value = address;
                  modal.find('.modal-body #address').val(address)
                  modal.find('.modal-footer #btn-submit').text('Update')
                  $('#formModal form').attr('action', url);
                  modal.find('.modal-body #method').html('{{ method_field('PATCH') }}')
              } else {
                  $('#formModal form')[0].reset();
                  $('#formModal [name=_method]').val('post');
                  modal.find('#modal-title').text("Tambah Data Penduduk")
                  modal.find('.modal-body #name').val()
                  modal.find('.modal-body #gender').val()
                  modal.find('.modal-body #place_of_birth').val()
                  modal.find('.modal-body #date_of_birth').val()
                  modal.find('.modal-body #address').val()
                  modal.find('.modal-footer #btn-submit').text('Submit')
                  modal.find('.modal-body #method').html('')
              }
            });
  </script>
@endpush