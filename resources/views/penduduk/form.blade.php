<!-- Modal -->
<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"  data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-md" style="width:95%;" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-title">Tambah Data Satuan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div id="method"></div>
                    <div class="form-group row">
                        <label class="col-md-3 col-md-offset-1" for="name">Nama Penduduk</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="name" name="name">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-3 col-md-offset-1" for="name">Jenis Kelamin</label>
                        <div class="col-md-9">
                            <select name="gender" id="gender" class="form-control">
                                <option value="-" selected disabled>-- Pilih --</option>
                                <option value="L">Laki-Laki</option>
                                <option value="P">Perempuan</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-3 col-md-offset-1" for="place_of_birth">Tempat Lahir</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="place_of_birth" name="place_of_birth">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-3 col-md-offset-1" for="date_of_birth">Tanggal lahir</label>
                        <div class="col-md-9">
                            <input type="date" class="form-control" id="date_of_birth" name="date_of_birth">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label class="col-md-3 col-md-offset-1" for="address">Alamat</label>
                        <div class="col-md-9">
                            <textarea name="address" id="address" cols="30" rows="10" class="form-control"></textarea>
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label class="col-md-3 col-md-offset-1" for="photo">Photo</label>
                        <div class="col-md-9">
                            <input type="file" name="photo" id="photo">
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" id="btn-submit">Simpan Data</button>
            </div>
        </div>
    </div>
</div>
</form>