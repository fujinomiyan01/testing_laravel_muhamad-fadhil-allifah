<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class UsersController extends Controller
{
    public function vRegister(){
        return view('register');
    }

    public function register(Request $request){
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'password' => 'required'
        ]);

        User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);

        return redirect('/')->with('success', 'Register Berhasil!');
    }

    public function vLogin(){
        return view('login');
    }

    public function login(Request $request){
        $user = $request->validate([
            'email' => ['required'],
            'password' => ['required'],
        ]);

        // return (strtotime($getPerusahaan->expiredDate) < strtotime(date('Y-m-d')));
        if(Auth::attempt($user)){
            $request->session()->regenerate();
            return redirect('/penduduk')->with('success', 'Anda telah login');
        } else {
            return back()->with(['error' => 'Email atau Password tidak sesuai']);
        }

        throw ValidationException::withMessages([
            'email' => 'Your provide credentials does not match our records',
        ]);
    }

    public function logout(Request $request) {
        // return $request;
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/')->with('success', 'Logout Berhasil');
   }
}
