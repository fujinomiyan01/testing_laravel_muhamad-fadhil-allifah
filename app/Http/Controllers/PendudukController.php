<?php

namespace App\Http\Controllers;

use App\Models\Penduduk;
use App\Http\Requests\StorePendudukRequest;
use App\Http\Requests\UpdatePendudukRequest;
use Illuminate\Support\Str;

class PendudukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['penduduk'] = Penduduk::leftJoin('t_users as u', 'u.id', 't_penduduks.user_id')->select('t_penduduks.id', 't_penduduks.name', 't_penduduks.gender', 't_penduduks.place_of_birth', 't_penduduks.date_of_birth', 't_penduduks.address', 't_penduduks.photo', 'u.name as nama_user', 'u.email as email_user')->get();  
        return view('penduduk.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorePendudukRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePendudukRequest $request)
    {
        $penduduk = new Penduduk();

        if($request->photo){
            $request->validate([
                'image' => 'image|mimes:jpg,png,jpeg,gif,svg|max:4096',
            ]);

            $getMime = $request->file('photo')->getMimeType(); 
            $explodedMime = explode('/' ,$getMime);
            $mime = end($explodedMime);
            $name = Str::random(25) . '.' . $mime;
            $request->photo->move('assets/img', $name);

            $penduduk->photo = ('/assets/img/' . $name);
        } else {
            $penduduk->photo = "-";
        }
        $penduduk->user_id = auth()->user()->id;
        $penduduk->name = $request->name;
        $penduduk->gender = $request->gender;
        $penduduk->place_of_birth = $request->place_of_birth;
        $penduduk->date_of_birth = $request->date_of_birth;
        $penduduk->address = $request->address;
        $penduduk->save();

        return back()->with('success', 'Input berhasil');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Penduduk  $penduduk
     * @return \Illuminate\Http\Response
     */
    public function show(Penduduk $penduduk)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Penduduk  $penduduk
     * @return \Illuminate\Http\Response
     */
    public function edit(Penduduk $penduduk)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatePendudukRequest  $request
     * @param  \App\Models\Penduduk  $penduduk
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePendudukRequest $request, Penduduk $penduduk)
    {
        if($request->photo){
            $request->validate([
                'image' => 'image|mimes:jpg,png,jpeg,gif,svg|max:4096',
            ]);

            $getMime = $request->file('photo')->getMimeType(); 
            $explodedMime = explode('/' ,$getMime);
            $mime = end($explodedMime);
            $name = Str::random(25) . '.' . $mime;
            $request->photo->move('assets/img', $name);

            $penduduk->photo = ('/assets/img/' . $name);
        } else {
            $penduduk->photo = $penduduk->photo;
        }
        $penduduk->user_id = auth()->user()->id;
        $penduduk->name = $request->name;
        $penduduk->gender = $request->gender;
        $penduduk->place_of_birth = $request->place_of_birth;
        $penduduk->date_of_birth = $request->date_of_birth;
        $penduduk->address = $request->address;
        $penduduk->update();
        // return redirect('/satuan')->with('success', 'Update Data berhasil');
        return redirect()->back()->with(['success' => 'Data Berhasil Diupdate!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Penduduk  $penduduk
     * @return \Illuminate\Http\Response
     */
    public function destroy(Penduduk $penduduk)
    {
        $penduduk->delete();
        return response(null, 204);
    }
}
