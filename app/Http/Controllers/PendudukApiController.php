<?php

namespace App\Http\Controllers;

use App\Helpers\ApiFormatter;
use App\Models\Penduduk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PendudukApiController extends Controller
{
    public function index()
    {
        $data = Penduduk::leftJoin('t_users as u', 'u.id', 't_penduduks.user_id')->select('t_penduduks.*', 'u.name as nama_penduduk')->get();

        if($data) {
            return ApiFormatter::createApi(200, 'success', $data);
        } else{
            return ApiFormatter::createApi(400,'Tidak ada Data');
        }
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'user_id' => 'required',
                'name' => 'required',
                'gender' => 'required',
                'place_of_birth' => 'required',
                'date_of_birth' => 'required',
                'address' => 'required',
                'photo' => 'required',
            ]);

            if($request->photo){
                $request->validate([
                    'image' => 'image|mimes:jpg,png,jpeg,gif,svg|max:4096',
                ]);
    
                $getMime = $request->file('photo')->getMimeType(); 
                $explodedMime = explode('/' ,$getMime);
                $mime = end($explodedMime);
                $name = Str::random(25) . '.' . $mime;
                $request->photo->move('assets/img', $name);
    
                $photo = ('/assets/img/' . $name);
            } else {
                $photo = '-';
            }

            $data = Penduduk::create([
                'user_id' => $request->user_id,
                'name' => $request->name,
                'gender' =>  $request->gender,
                'place_of_birth' =>  $request->place_of_birth,
                'date_of_birth' =>  $request->date_of_birth,
                'address' =>  $request->address,
                'photo' => $photo,
            ]);

            DB::commit();

            if($data) {
                return ApiFormatter::createApi(200, 'success', $data);
            } else{
                return ApiFormatter::createApi(400,'Kesalahan Query');
            }
        } catch (\Exception $error) {
            DB::rollBack();
            return ApiFormatter::createApi(400,'Kesalahan Query '. $error->getMessage());
        }
    }

    public function show($id)
    {
        $data = Penduduk::where('id', $id)->first();
        
        if($data) {
            return ApiFormatter::createApi(200, 'success', $data);
        } else{
            return ApiFormatter::createApi(400,'Tidak ada Data');
        }
    }

    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $data = Penduduk::findOrFail($id);
            if($request->photo){
                $request->validate([
                    'image' => 'image|mimes:jpg,png,jpeg,gif,svg|max:4096',
                ]);
    
                $getMime = $request->file('photo')->getMimeType(); 
                $explodedMime = explode('/' ,$getMime);
                $mime = end($explodedMime);
                $name = Str::random(25) . '.' . $mime;
                $request->photo->move('assets/img', $name);
    
                $photo = ('/assets/img/' . $name);
            } else {
                $photo = $data->photo;
            }

            $data->update([
                'user_id' => $request->user_id,
                'name' => $request->name,
                'gender' =>  $request->gender,
                'place_of_birth' =>  $request->place_of_birth,
                'date_of_birth' =>  $request->date_of_birth,
                'address' =>  $request->address,
                'photo' => $photo,
            ]);
            

            DB::commit();

            if($data) {
                return ApiFormatter::createApi(200, 'success', $data);
            } else{
                return ApiFormatter::createApi(400,'Kesalahan Query');
            }
    
        }catch (\Exception $error) {
            DB::rollBack();
            return ApiFormatter::createApi(400,'Kesalahan Query '. $error->getMessage());
        }
    }

    public function destroy($id)
    {

        DB::beginTransaction();
        try {
            $penduduk = Penduduk::findOrFail($id);

            $data = $penduduk->delete();
            DB::commit();

            if($data) {
                return ApiFormatter::createApi(200, 'success', $data);
            } else{
                return ApiFormatter::createApi(400,'Kesalahan Query');
            }
        } catch (\Exception $error) {
            DB::rollBack();
            return ApiFormatter::createApi(400,'Kesalahan Query '. $error->getMessage());
        }
    }
}
